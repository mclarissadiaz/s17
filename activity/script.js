//console.log("hello world")

let students=[];
console.log(students);

function addStudent(element){
	students.push(element);
	var results = "";
	results += element + " has been added to the student's list."
	return results;
};
console.log(addStudent("Joe"));
console.log(addStudent("John"));
console.log(addStudent("Jane"));
//console.log(students);

function countStudent(){
	students.length();
};
console.log("There are a total of " + students.length + " students enrolled.");
//console.log(students.length);

function sortStudents(){
	students.sort()
};
sortStudents();
//console.log(students);

students.forEach(
	function printStudents(element){
		console.log(element)
	}
);

function findStudent(arr, query) {
  return arr.filter(function(el) {
    return el.toLowerCase().indexOf(query.toLowerCase()) !== -1
  })
}

console.log(findStudent(students, 'joe') + " is an enrollee.")
console.log(findStudent(students, 'bill') + "No student found with the name.")
console.log(findStudent(students, 'j') + " are enrollees.")

let mapStudents = students.map(
	function printStudents(element){
		return `${element} - Section A`
	}
)
console.log(mapStudents);

let remove = function removeStudent(){
	students.splice(1, 0);
	return (`${remove} was removed from the student's list.`);
};
