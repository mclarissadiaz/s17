/*
Miniactivity
*/
console.log("drink html");
console.log("eat javascript");
console.log("inhale css");
console.log("bake bootstrap");


/*
ARRAY - used to store multiple  related datas in 1 variable. Declared using square brackets [].

SYNTAX:
	let/const<arrayName>=["elementA", "elementB",... "elementN"];

arrays are used if there is a need to manipulate repated values stored in it.

index - position of each element in the array.

REMINDER: index starts count at 0.
formula: nth element -1/ arrayName.length -1
*/

let tasks = ["drink html", "eat javascript", "inhale css", "bake bootstrap"];
console.log(tasks);

console.log(tasks[2]); //to access elements in array. calling an element through index is by using SYNTAX:
//arrayName[index#]

/*
let indexOfLastElement = tasks.length -1;
console.log(indexOfLastElement);
*/

//getting the number of elements
console.log(tasks.length);

//accessing index of non-existing element would result to undefined
console.log(tasks[4]);
//--------------------------------------
//ARRAY MANIPULATION

//Manipulating the End of Array

//Adding an element
let numbers = ["one", "two", "three", "four"];
console.log(numbers);
//console.log(numbers[4]);

//Using Assignment Operator - Add small # of elements
numbers[4] = "five";
console.log(numbers);

//Push Method
	//add element at end of array
numbers.push("element");
console.log(numbers);
	//callback function - function thats passed on/inserted to another function. done bec the inserted function is following a particular syntax & dev is trying to simplify that syntax by just inserting it inside another function
function pushMethod(element){
	numbers.push(element);
}
pushMethod("six");
pushMethod("seven");
pushMethod("eight");
console.log(numbers);

//REMOVING OF AN ELEMENT
//Pop method - removing the last element in the array
numbers.pop()
console.log(numbers);

function popMethod(){
	numbers.pop()
}
popMethod();
/*popMethod();
popMethod();
popMethod();
popMethod();
popMethod();*/
console.log(numbers);

//---------------------------------------------
//Manipulating the Beginning of the Array
//Remove 1st element
//Shift Method - removing 1st element in an array
numbers.shift();
console.log(numbers);

//callback function
function shiftMethod(){ //no need to put placeholder bec the function will select the whole array and take out the 1st element.
	numbers.shift();
}
shiftMethod();
console.log(numbers);

//-------------------------------------------
//Adding an Element
//Unshift Method
numbers.unshift("zero");
console.log(numbers);

//Callback Function
//function unshiftMethod(<parameter/argument>){ //element - is a placeholder element.
function unshiftMethod(element){ //element - is a placeholder element.
	numbers.unshift(element);
}
unshiftMethod("mcdo");
console.log(numbers);

//Arrangement of the Elements

let numbs = [15, 27, 32, 12, 6, 8, 236]
console.log(numbs);

//Sort Method - arranges elements in ascending or descending order; has anonymous functioninside that has 2 parameters.
	//anonymous function - unnamed function & can only be used once
	/*
	2 parameters in anonymous function represents:
		1st parameter - first/smallest element
		2nd parameter - last/biggest element
		doesnt have to be (a, b); can be words or other letters.
	*/
/*
SYNTAX:
	arrayName.sort(
		function (a, b){
			a - b -ascending
			b - a -descending
		}
	)
*/
numbs.sort(
	function(a, b){
		return a - b
	}
);
console.log(numbs);

//descending order
numbs.sort(
	function(a, b){
		return b - a
	}
)
console.log(numbs);

//Reverse method - reverses the order of elements in an array but does not sort. it will depend on last order of array if its in random, descending, ascending.
numbs.reverse();
console.log(numbs);

//-------------------------------------------

//Splice Method like: ctrl+x ctrl+p
/*
	directly manipulates the array
	first parameter - the index of element from w/c the ommitting will begin
	second parameter - determines number of elements to be omitted (0, 2) 0 is index, 2 is # of elements to be omitted.
	3rd parameter onwards - the replacements for the removed elements (4,2, 31, 11, 111)

*/
//1 parameter
//let nums = numbs.splice(1); //(1) determines index from w/c ommitting will commence
//2 parameters
//let nums = numbs.splice(0, 2);
//3 parameters
let nums = numbs.splice(4, 2, 31, 11, 111);
console.log(numbs);
console.log(nums);

//---------------------------------------
//Slice Method like: ctrl+c ctrl+p
/*
	-does not affect original array; creates sub-array, no ommitting of any elelemt
	-1st parameter - index where the copying will begin
	-2 parameters - determines # of elements to be ommitted
	SYNTAX:
	let/const <newArray> = <originalArrayName>.slice(firstParameter, secondParameter)

	-3 parameter onwards - replaces for the  removed elements
*/
//1 parameter
//let slicedNums = numbs.slice(1);

//2 parameters
let slicedNums = numbs.slice(2, 7); //index 2(counting from 0), 7th element from start of array (counting from 1 for first element, with 2nd index as start of copying upto 7th element)
console.log(numbs);
console.log(slicedNums);

//-----------------------------------
//Merging of Array
//Concat
console.log(numbers);
console.log(numbs);
let animals = ["dog", "tiger", "kangaroo", "chicken"];
console.log(animals);

let newConcat = numbers.concat(numbs, animals);
console.log(newConcat);
console.log(numbers);
console.log(numbs);
console.log(animals);

//Join Method - merges elements in array & makes them string data.
//parameters - separators (space, dash, & nothing at all)
let meal = ["rice", "steak", "juice"];
console.log(meal);

newJoin = meal.join();
console.log(newJoin);

newJoin = meal.join("");
console.log(newJoin);

newJoin = meal.join(" ");
console.log(newJoin);

newJoin = meal.join("-");
console.log(newJoin);

//toString Method
console.log(nums);
console.log(typeof nums); //typeof determines datatype beside it

let newString = nums.toString();
console.log(newString);       
console.log(typeof newString);

//Accessors
let countries = ["US", "PH", "JP", "HK", "SG", "PH", "NZ"];
//indexOf - returns 1st index it finds from beginning of array
/*
SYNTAX:
arrayName.indexOf()

*/
let index = countries.indexOf("PH")
console.log(index);


//finding a non existing element - returns -1
index = countries.indexOf("AU");
console.log(index);

//lastIndexOf() - finds index of element from end of array 
/*
SYNTAX:
indexOf arrayName.indexOf()
*/
index = countries.lastIndexOf("PH");  
console.log(index);
//returns -1 for non existing elements (case sensitive)
index = countries.lastIndexOf("Ph");  
console.log(index);
/*
using selection control structure

if (countries.indexOf("CAN") === -1) {
	console.log("Element not existing");
}else{
	console.log("Element exists in the array");
}
*/

//Iterators

let days = ["mon", "tue", "wed", "thu", "fri", "sat", "sun"]
console.log(days)

//forEach - returns(performs the statement in) each element in the array
/*
array.forEach(
	function(element){
	statement/s
	}
)
*/
days.forEach(
	function(element){
		console.log(element)
	}
)
//Map
/*
	returns a copy of array from original w/c can be manipulated
	SYNTAX
	array.map(
	function(element){
	return `${element} additional message`
	}
	)
*/
let mapDays = days.map(
	function(element){
		return `${element} is the day of the week`
	}
)
console.log(mapDays);
console.log(days);

//Filter - filters elements & copies them into another array
console.log (numbs);
let newFilter = numbs.filter(
		function (element) {
			return element < 30;
		}
)
console.log(newFilter);
console.log(numbs);

//Includes - return true (boolean)if element/s are inside the array

let animalIncludes = animals.includes("dog");
console.log(animalIncludes);

//Every - (another boolean)checks if ALL elements pass the condition (returns true if all elements pass)
console.log(nums)
let newEvery = nums.every(
		function (element){
			return (element > 10);
		}
	)
console.log(newEvery);

//Some - checks AT LEAST 1 element passes the condition (boolean)
let newSome = nums.some(
	function(element){
		return(element > 30)
	}
)
console.log(newSome);

nums.push(50)
console.log(nums);
//Reduce - performs operation in all of the elements in the array
/*
1st element - 1st element
2nd parameter - last element
*/
let newReduce = nums.reduce(
 		function ( a, b ) {
 			return a + b
 			//return b - a //start at end of element to the first of element UPDATE TO FOLLOW....
 		}
 	)
console.log(newReduce);

let average = newReduce/nums.length
console.log(average)

//toFixed
console.log(average)
console.log(average.toFixed(2)); //rounded to (2) decimal place 
/*
parseint - rounds # to nearest whole #
parsefloat - rounds # to nearest target decimal place (through .toFixed) *numerical
.toFixed - *string
*/
console.log(parseInt(average.toFixed(2)));  
console.log(parseFloat(average.toFixed(2)));  